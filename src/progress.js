function drawProgressBar(progress) {
  const barLength = 50;
  const filledLength = Math.round(barLength * progress);
  const emptyLength = barLength - filledLength;
  const progressBar =
    "[" + "=".repeat(filledLength) + " ".repeat(emptyLength) + "]";
  process.stdout.clearLine();
  process.stdout.cursorTo(0);
  process.stdout.write(progressBar + " " + (progress * 100).toFixed(2) + "%");
}

function progressBar(totalSteps) {
  let progress = 0;
  const interval = setInterval(() => {
    progress += 1 / totalSteps;
    drawProgressBar(progress);
    if (progress >= 1) {
      clearInterval(interval);
      process.stdout.write("\n");
    }
  }, 100);
}
export default progressBar;
