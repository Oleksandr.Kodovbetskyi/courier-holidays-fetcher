import { convertArrayToCSV } from "convert-array-to-csv";
import { appendFileSync, createReadStream, existsSync, readFile } from "fs";
import { appendFile } from "fs/promises";
import csv from "csv-parser";
import progressBar from "./progress.js";
import fetch from "node-fetch";
import { config } from "../config.js";
const {
  COURIERS_SOURCE,
  HOLIDAYS_START_DATE,
  HOLIDAYS_END_DATE,
  HOLIDAYS_YEAR,
  OUTPUT_NAME,
  REQ_RATE,
} = config;

console.log(">>>>>>>>>>>>>>>>>>LOADDED CONFIG<<<<<<<<<<<<<<<<<<<<< ");
console.log(
  COURIERS_SOURCE,
  HOLIDAYS_START_DATE,
  HOLIDAYS_END_DATE,
  HOLIDAYS_YEAR,
  OUTPUT_NAME,
  REQ_RATE
);
const courier_ids = [];

function appendToCSV(success, data) {
  if (success.status) {
    const parseJson = JSON.parse(data);
    const header = Object.keys(parseJson);
    const values = Object.values(parseJson);
    createAndWriteFile(`${OUTPUT_NAME}-success`, header, values);
  } else {
    createAndWriteFile(`${OUTPUT_NAME}-fails`, ["CR_ID"], [data]);
  }
}

function createAndWriteFile(output, header, values) {
  try {
    if (!existsSync(`output/${output}.csv`)) {
      appendFileSync(`output/${output}.csv`, arrayToCSV(header));
      appendFileSync(`output/${output}.csv`, arrayToCSV(values));
    } else {
      appendFileSync(`output/${output}.csv`, arrayToCSV(values));
    }
  } catch (err) {
    console.log("Couldn't write file", values);
  }
}

function arrayToCSV(array) {
  let csvContent = "";
  for (let i = 0; i < array.length; i++) {
    if (i > 0) {
      csvContent += ",";
    }
    csvContent += `"${array[i]}"`;
  }
  csvContent += "\n";
  return csvContent;
}

const fetchHolidays = async (id) => {
  try {
    const REQUEST_URL = `http://enterprise-services2.prod.hermescloud.co.uk/holiday-pay-service/v1/balance/courier/${id}/year/${HOLIDAYS_YEAR}?endDate=${HOLIDAYS_END_DATE}&startDate=${HOLIDAYS_START_DATE}`;

    const req = await fetch(REQUEST_URL);

    if (req.ok) {
      const body = await req.text();
      appendToCSV({ status: true }, body);
    } else {
      throw new Error("Couldn't fetch");
    }
  } catch (err) {
    appendToCSV({ status: false }, id);
  }
};

createReadStream(`input/${COURIERS_SOURCE}`)
  .pipe(csv())
  .on("data", function (row) {
    courier_ids.push(row.CR_ID);
  })
  .on("end", () => {
    progressBar(courier_ids.length);
    courier_ids.forEach((id, index) => {
      setTimeout(() => {
        fetchHolidays(id);
        // console.log("Processing request no.", index);
        //console.log("Courier id", id);
      }, index * REQ_RATE);
    });
  });
