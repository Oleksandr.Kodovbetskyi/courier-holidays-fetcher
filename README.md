# Courier Holidays Extractor

## Description
This is a script to extract courier holidays information from the following endpoint:
[http://enterprise-services2.prod.hermescloud.co.uk/holiday-pay-service/v1/balance/courier/444136/year/2023?endDate=2024-02-25&startDate=2024-02-27](http://enterprise-services2.prod.hermescloud.co.uk/holiday-pay-service/v1/balance/courier/444136/year/2023?endDate=2024-02-25&startDate=2024-02-27).

It produces a `.csv` file in the output directory with the exported courier holidays.

## Requirements
- Node.js v20 or higher. You can download it [here](https://nodejs.org/en).

## How to Use the Script

### Step 1: Clone the Repository
Clone the repository to your local machine.

### Step 2: Install Dependencies
Open the cloned folder with Visual Studio Code, open the Terminal, and run:

```bash 
npm install
```

### Step 3: Export Courier IDs
Export the CR_IDs of all active couriers with courier type SE+ or Worker from the database using the following query:

```bash
SELECT CR_ID FROM PNET.CR WHERE CR_TYP_ID IN ('CRar', 'Cras') AND STAT_ID = 'A';
```
### Step 4: Place Exported File
Place the exported file in the directory courier-holidays-fetcher/input.

### Step 5: Edit Configuration
Edit the config.js file to set the following parameters:

- COURIERS_SOURCE: The name of the exported file with courier IDs.
- HOLIDAYS_START_DATE: The start date for the holidays.
- HOLIDAYS_END_DATE: The end date for the holidays.
- HOLIDAYS_YEAR: The year for the holidays.
- OUTPUT_NAME: The name of the output file.
- REQ_RATE: The rate of requests per second. Recommended value is 1000, but a lower value like 100 can speed up the process.

#### Example configuration:
```bash 
export const config = {
  COURIERS_SOURCE: "CR_202405201800.csv",
  HOLIDAYS_START_DATE: "2024-04-03",
  HOLIDAYS_END_DATE: "2025-03-01",
  HOLIDAYS_YEAR: "2024",
  OUTPUT_NAME: "courier_holidays-21-05-2024",
  REQ_RATE: 100,
};
```

Save the configuration changes.

### Step 6: Start Exporting
To start the exporting process, run:

```bash
npm run start
```

This will start the process and create a .csv file with the name specified in OUTPUT_NAME. The file will include a success file and a fail file, where the fail file contains couriers that failed, usually due to expired contracts.

## PLEASE NOTE

- If you start the script with the same name twice, it will duplicate data into the previous file. Therefore, the old file needs to be deleted, or `OUTPUT_NAME` should be changed.
- If your laptop goes into sleep mode, the export process will not continue.
- If you haven't added couriers to the `courier-holidays-fetcher/input` directory and changed `COURIER_SOURCE`, the script will crash.
- If you are not connected to a VPN, it won't be able to fetch couriers, and all couriers will be logged into the `fails` file.
